;; Copyright: 2024 TIlman Andre Mix
;; SPDX-License-Identifier: 0BSD

(set-charset-priority 'unicode) ;; utf8 in every nook and cranny
(setq locale-coding-system 'utf-8
      coding-system-for-read 'utf-8
      coding-system-for-write 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)
(setq default-process-coding-system '(utf-8-unix . utf-8-unix))

(setq inhibit-startup-message t
      inhibit-splash-screen t)
(scroll-bar-mode -1) ; disable scrollbar
(tool-bar-mode -1) ;; disable tool bar
(tooltip-mode -1) ;; disable tooltip
(set-fringe-mode 10) ;;set fringe to 10
(menu-bar-mode -1) ; disable menu bar
(blink-cursor-mode 0)
(setq use-short-answers t)
(setq-default tab-width 4)
(setq-default indent-tabs-mode nil)
(setq ring-bell-function 'ignore)
(global-visual-line-mode t)
(setq select-enable-clipboard t)
(recentf-mode 1)
(setq tab-always-indent 'complete)
(setq show-trailing-whitespace 1)

;; Backups are annyoing
(setq make-backups-files nil)
(setq auto-save-default nil)

;; less noise when compiling elisp
(setq byte-compile-warnings '(not free-vars unresolved noruntime lexical make-local))
(setq load-prefer-newer t)

;; keep backup and save files in a dedicated directory
(setq backup-directory-alist
      `((".*" . ,(concat user-emacs-directory "backups")))
      auto-save-file-name-transforms
      `((".*" ,(concat user-emacs-directory "backups") t)))

;; User config
(setq user-full-name "Tilman A. Mix")
(setq user-mail-address "tilmanmixyz@proton.me")

;; Custom File
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load custom-file 'noerror)

;; Frame Title
(setq-default frame-title-format '("Emacs - %b"))

(global-display-line-numbers-mode 1) ; enable line numbers
(setq display-line-numbers-type 'relative)
(dolist (mode '(org-mode-hook
                term-mode-hook
                vterm-mode-hook
                eat-mode-hook
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(setq initial-major-mode 'lisp-data-mode)
(setq initial-scratch-message "")

(defalias 'yes-or-no-p 'y-or-n-p)
(setq read-extended-command-predicate #'command-completion-default-include-p)

;; Font
;; (defvar cfg/font-spec-size 15)
;; (defvar cfg/variable-font-spec-size 14)
;; (defvar cfg/font "CaskaydiaCove Nerd Font")
;; (defvar cfg/variable-font "Overpass")
;; 
;; (set-frame-font (font-spec :family cfg/font :size cfg/font-spec-size))
;; (add-to-list 'default-frame-alist '(font . "CaskaydiaCove Nerd Font"))
(provide 'tea-core)
;;; tea-core.el ends here
