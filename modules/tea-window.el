;; Copyright: 2024 Tilman Andre Mix
;; SPDX-License-Identifier: 0BSD

(setup (:elpaca windresize)
  (:after general)
  (:bind-into windresize-map
    "h" windresize-left
    "j" windresize-down
    "k" windresize-up
    "l" windresize-right)

  (defun tea/vsplit-follow ()
    "Split Window to right and follow using `windmove'."
    (interactive)
    (split-window-right)
    (windmove-right 1))
  (defun tea/split-follow ()
    "Split Window to bottom and follow using `windmove'."
    (interactive)
    (split-window-below)
    (windmove-down 1))

  (tea/leader
    ;; Window
    "w" '(nil :which-key "Window")
    "wv" '(tea/vsplit-follow :which-key "Split to right side")
    "ws" '(tea/split-follow :which-key "Split to bottom")
    "wc" '(delete-window :which-key "Close current window")
    "wh" '(windmove-left :which-key "Move to left window")
    "wj" '(windmove-down :which-key "Move to bottom window")
    "wk" '(windmove-up :which-key "Move to upper window")
    "wl" '(windmove-right :which-key "Move to right window")
    "wr" '(windresize :which-key "Resize window")
    )
  )

(provide 'tea-window)
;;; tea-window.el ends here
