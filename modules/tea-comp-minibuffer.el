;; Copyright: 2024 Tilman Andre MIx
;; SPDX-License-Identifier: 0BSD

(setup vertico
  (:elpaca t)
  (vertico-mode 1))

(setup orderless
  (:elpaca t)
  (:after vertico)
  (:option completion-styles '(orderless partial-completion basic)
           completion-category-defaults nil
           completion-category-overrides nil))

(setup consult
  (:elpaca t)
  (:after general)
  (tea/leader
    ;; File
    "fr" '(consult-recent-file :which-key "recent files")
    ;; Buffer
    "bb" '(consult-buffer :which "Switch buffer")
    ))

(setup affe
  (:elpaca t)
  (:after general)
  (tea/leader
    "fF" '(affe-find :which-key "regex find files")
    "fw" '(affe-grep :which-key "ripgrep")
    ))

(provide 'tea-comp-minibuffer)
;;; tea-comp-minibuffer.el ends here
