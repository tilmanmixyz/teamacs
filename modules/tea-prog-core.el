;; Copyright: 2024 Tilman Andre Mix
;; SPDC-License-Identifier: 0BSD

(setup electric
  (:elpaca nil)
  (electric-pair-mode 1))

(setup rainbow-delimiters
  (:elpaca t)
  (:hook-into prog-mode))


(show-paren-mode 1)

(setup project
  (:elpaca nil)
  (:after general)
  (tea/leader
    ;; Projects
    "p" '(nil :which-key "projects")
    "pp" '(project-switch-project :which-key "switch project")
    "pf" '(project-find-file :which-key "find file")
    "pd" '(project-find-dir :which-key "find dir")
    "pD" '(project-dired :which-key "open dired")
    "pb" '(project-switch-to-buffer :which-key "switch buffer")
    "pe" '(project-eshell :which-key "eshell")
    ))

(setup (:elpaca flycheck)
  (:hook-into prog-mode))

(provide 'tea-prog-core)
;;; tea-prog-core.el ends here
