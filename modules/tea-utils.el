;; Copyright: 2024 TIlman Andre Mix
;; SPDX-License-Identifier: 0BSD

(setup (:elpaca simpleclip)
  (:bind-into global-map
    "C-S-v" simpleclip-paste
    "C-S-c" simpleclip-copy)
  (defun tea/paste-in-minibuffer ()
    "Paste in minibuffer using `simpleclip'"
    (local-set-key (kbd "C-S-v") 'simpleclip-paste)
    (local-set-key (kbd "M-v") 'simpleclip-paste)
    )
  (add-hook 'minibuffer-setup-hook 'tea/paste-in-minibuffer)
  (simpleclip-mode 1))

(setup (:elpaca deadgrep))

(setup (:elpaca undo-fu)
  (:after evil)
  (evil-set-undo-system 'undo-fu))

(setup (:elpaca vundo))

(provide 'tea-utils)
;;; tea-utils.el ends here
