;; Copyright: 2024 Tilman Andre Mix
;; SPDX-License-Identifier: 0BSD

(setup
  (:elpaca eat :repo "https://codeberg.org/akib/emacs-eat.git")
  (:after general)
  (with-eval-after-load 'project
    (push '(?t "Terminal" eat-project) project-switch-commands))
  (tea/leader
    "pt" '(eat-project-other-window :which-key "Open Terminal")
    )
  )

(provide 'tea-terminal)
;;; tea-terminal.el ends here
