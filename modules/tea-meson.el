;; Copyright: 2024 Tilman Andre Mix
;; SPDX-License-Identifier: 0BSD

(setup (:elpaca meson-mode))

(provide 'tea-meson)
;;; tea-meson.el ends here
