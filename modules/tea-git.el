;; Copyright: 2024 TIlman Andre Mix
;; SPDX-License-Identifier: 0BSD

(setup (:elpaca magit)
  (:after general)
  (tea/leader
    ;; Git
    "g" '(nil :which-key "Git")
    "gs" '(magit-status :which-key "Status")
    )
  )

(provide 'tea-git)
;;; tea-git.el ends here
