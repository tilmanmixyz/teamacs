;; Copyright: 2024 Tilman Andre Mix
;; SPDX-License-Identifier: 0BSD

(setup (:elpaca doom-themes :host github :repo "tilmanmixyz/doom-theme-gruvbox")
  (:option doom-themes-enable-bold t
           doom-themes-enable-italic t)
  (doom-themes-visual-bell-config)
  (doom-themes-org-config)
  (load-theme 'doom-gruvbox t))

(setup (:elpaca minions)
  (minions-mode))

(setup (:elpaca tea-line :repo "https://codeberg.org/tilmanmixyz/tea-line.git")
       (tea-line-mode 1))

(set-face-font 'default "CaskaydiaCove Nerd Font-12")

(provide 'tea-ui)
;;; tea-ui.el ends here
