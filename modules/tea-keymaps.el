;; Copyright: 2024 Tilman Andre Mix
;; SPDX-License-Identifier: 0BSD

(setup (:elpaca general)
  (general-evil-setup)

  (general-create-definer tea/leader
    :states '(normal visual motion)
    :keymaps 'override
    :prefix "SPC"
    :global-prefix "C-SPC"
    )

  (tea/leader
    ;; Top Level
 "/" '(comment-line :which-key "Comment Line")
 "SPC" '(execute-extended-command :which-key "M-x")
 "q" '(kill-emacs :which-key "Kill Emacs")

 ;; Dired
 "d" '(nil :which-key "dired")
 "dd" '(dired :which-key "choose dir")
 "dj" '(dired-jump :which-key "dired in dir of current buffer")

 ;; File
 "f" '(nil :which-key "Files")
 "ff" '(find-file :which-key "find files")
 "fs" '(save-buffer :which-key "save file")
 "fR" '(rename-buffer :which-key "rename file")

 ;; Help/emacs
 "h" '(nil :which-key "help/emacs")
 "hv" '(describe-variable :which-key "des. variable")
 "hb" '(describe-bindings :which-key "des. bindings")
 "hM" '(describe-mode :which-key "des. mode")
 "hf" '(describe-function :which-key "des. func")
 "hF" '(describe-face :which-key "des. face")
 "hk" '(describe-key :which-key "des. key")

 "hm" '(nil :which-key "switch mode")
 "hme" '(emacs-lisp-mode :which-key "elisp mode")
 "hmo" '(org-mode :which-key "org mode")
 "hmt" '(text-mode :which-key "text mode")

 "hi" '(info :which-key "Info (documentation)")

 ;; Buffer
 "b" '(nil :which-key "Buffer")
 "bd" '(kill-this-buffer :which-key "Kill this buffer")

 ;; Toggles
 "t" '(nil :which-key "toggles")
 "tt" '(toggle-truncate-lines :which-key "truncate lines")
 "tv" '(visual-line-mode :which-key "visual line mode")
 "tn" '(display-line-numbers-mode :which-key "display line numbers")
 "th" '(load-theme :which-key "load theme")
 "td" '(disable-theme :which-key "disable theme")
))

(provide 'tea-keymaps)
;;; tea-keymaps.el ends here
