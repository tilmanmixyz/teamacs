;; Copyright: 2024 Tilman Andre Mix
;; SPDC-License-Identifier: 0BSD

(setup corfu
  (:elpaca t)
  (:option corfu-auto t
           corfu-cycle t)
  (global-corfu-mode 1))

(setup cape
  (:elpaca t)
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-dabbrev))

(setup kind-icon
  (:elpaca t)
  (:after corfu)
  (if (display-graphic-p)
      (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter)
    ))

(provide 'tea-comp-graphical)
;;; tea-comp-graphical.el ends here
