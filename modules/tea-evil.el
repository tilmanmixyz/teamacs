;; Copyright: 2024 Tilman Andre Mix
;; SPDX-License-Identifier: 0BSD

(setup evil
  (:elpaca t)
  (:option
   evil-want-C-u-scroll t
   evil-want-C-d-scroll t
   evil-want-integration t
   evil-want-keybinding nil
   evil-want-C-i-jump nil
   evil-vsplit-window-right t
   evil-split-window-below t
   )
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)
  )

(setup evil-collection
  (:elpaca t)
  (:after evil)
  (evil-collection-init))

(setup (:elpaca evil-numbers)
 (define-key evil-normal-state-map (kbd "C-A")
  'evil-numbers/inc-at-pt)
 (define-key evil-normal-state-map (kbd "C-S-A")
  'evil-numbers/dec-at-pt))
(provide 'tea-evil)
;;; tea-evil.el ends here
