# teamacs - Emacs Config

## Copyright
© 2024 Tilman Andre Mix

## License
This configuration of GNU Emacs is licensed under the [BSD Zero Clause License (SPDX-License-Identifier: 0BSD)](https://spdx.org/licenses/0BSD.html)
