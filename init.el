;; Copyright: 2024 TIlman Andre Mix
;; SPDX-License-Identifier: 0BSD
;;; Code:
(defvar tea-dir (file-name-directory load-file-name)
  "The root dir of the Teamacs distribution.")
(defvar tea-modules-dir (expand-file-name "modules" tea-dir)
  "Modules for Teamacs functionality.")
(add-to-list 'load-path tea-modules-dir)

;; Setup Package Management and Configuration
(require 'tea-elpaca)
(require 'tea-setup)

;; Emacs Core Options Config
(require 'tea-core)

;; Keymaps
(require 'tea-keymaps)

;; Evil Config
(require 'tea-evil)

;; Completion
;;   Minibuffer
(require 'tea-comp-minibuffer)

;;   Graphical
(require 'tea-comp-graphical)

;; Core Programming
(require 'tea-prog-core)

;; Window Manipulation
(require 'tea-window)

;; Terminal
(require 'tea-terminal)

;; Appearance
(require 'tea-ui)

;; Utils
(require 'tea-utils)

;; Git / Magit
(require 'tea-git)

;; Prog langs
(require 'tea-meson)

(provide 'init)
;;; init.el ends here
